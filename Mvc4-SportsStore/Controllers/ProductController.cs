﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Models;
using SportsStore.Json;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {

        private IProductRepository repository;
        public int PageSize = 4;

        public ProductController(IProductRepository productRepository)
        {
            this.repository = productRepository;
        }

        
        public ActionResult List(string category,int page=1)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = repository.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductID)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems =  category == null? repository.Products.Count() : repository.Products.Where(e => e.Category == category).Count()
                },CurrentCategory = category
            };
            return View(model);
        }

        public FileContentResult GetImage(int productId)
        {
            Product prod = repository.Products.FirstOrDefault(p => p.ProductID == productId);
            if (prod != null)
            {
                return File(prod.ImageData, prod.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public JsonDotNetResult GetJsonProducts()
        {
            var products = repository.Products;

            List<Product> listado = products.ToArray().Select
                                    (c => { c.ImageData = null; return c; }).ToList();

            return new JsonDotNetResult(listado);
        
        }

        [HttpGet]
        public JsonDotNetResult Json(string id="")
        {
            switch (id)
            {
                case "Product":
                    return new JsonDotNetResult(repository.Products);                    
                default:
                    return null;

            }
            
        }
    }
}
