﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Entities;

namespace SportsStore.Binders
{
    public class CartModelBinder : IModelBinder
    {

        private const string sessionKey = "Cart";
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        { 
            //Obtenemos el Cart de la sesión
            Cart cart = (Cart)controllerContext.HttpContext.Session[sessionKey];

            //Creamos un carro si no estaba en session
            if (cart == null)
            {
                cart = new Cart();
                controllerContext.HttpContext.Session[sessionKey] = cart;

            }

            return cart;
        }

    }
}
